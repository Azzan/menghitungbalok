package com.example.ulanganrpl3_07;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private EditText et_panjang,et_lebar,et_tinggi;
    private double p,l,t;
    private Button kel,lu,vol;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        et_panjang=findViewById(R.id.etPanjang);
        et_lebar=findViewById(R.id.etLebar);
        et_tinggi=findViewById(R.id.etTinggi);
        kel=findViewById(R.id.btnKeliling);
        lu=findViewById(R.id.btnLuas);
        vol=findViewById(R.id.btnVolume);

        kel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (et_panjang.getText().toString().length()==0&et_lebar.getText().toString().length()==0&et_tinggi.getText().toString().length()==0){
                    et_panjang.setError("Required!");
                    et_lebar.setError("Required!");
                    et_tinggi.setError("Required!");
                }
                else if (et_lebar.getText().toString().length()==0&et_tinggi.getText().toString().length()==0){
                    et_lebar.setError("Required!");
                    et_tinggi.setError("Required!");
                }
                else if (et_panjang.getText().toString().length()==0&et_lebar.getText().toString().length()==0){
                    et_lebar.setError("Required!");
                    et_panjang.setError("Required!");
                }
                else if (et_panjang.getText().toString().length()==0&et_tinggi.getText().toString().length()==0){
                    et_tinggi.setError("Required!");
                    et_panjang.setError("Required!");
                }
                else if (et_panjang.getText().toString().length()==0){
                    et_panjang.setError("Required!");
                }
                else if (et_lebar.getText().toString().length()==0){
                    et_lebar.setError("Required!");
                }
                else if (et_tinggi.getText().toString().length()==0){
                    et_tinggi.setError("Required!");
                }
                else {
                    List<String>variabel=new ArrayList<>();
                    data(variabel);

                    String k="Keliling";
                    Double keliling=4*(p+l+t);

                    Intent intent=new Intent(getApplicationContext(),result.class);
                    intent.putExtra("char",String.valueOf(k));
                    intent.putExtra("data",String.valueOf(keliling));
                    startActivity(intent);
                }
            }
        });
        lu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (et_panjang.getText().toString().length()==0&et_lebar.getText().toString().length()==0&et_tinggi.getText().toString().length()==0){
                    et_panjang.setError("Required!");
                    et_lebar.setError("Required!");
                    et_tinggi.setError("Required!");
                }
                else if (et_lebar.getText().toString().length()==0&et_tinggi.getText().toString().length()==0){
                    et_lebar.setError("Required!");
                    et_tinggi.setError("Required!");
                }
                else if (et_panjang.getText().toString().length()==0&et_lebar.getText().toString().length()==0){
                    et_lebar.setError("Required!");
                    et_panjang.setError("Required!");
                }
                else if (et_panjang.getText().toString().length()==0&et_tinggi.getText().toString().length()==0){
                    et_tinggi.setError("Required!");
                    et_panjang.setError("Required!");
                }
                else if (et_panjang.getText().toString().length()==0){
                    et_panjang.setError("Required!");
                }
                else if (et_lebar.getText().toString().length()==0){
                    et_lebar.setError("Required!");
                }
                else if (et_tinggi.getText().toString().length()==0){
                    et_tinggi.setError("Required!");
                }
                else {
                    List<String>variabel=new ArrayList<>();
                    data(variabel);

                    String lu="Luas";
                    Double luas=2*(p*l+p*t+l*t);

                    Intent intent=new Intent(getApplicationContext(),result.class);
                    intent.putExtra("char",String.valueOf(lu));
                    intent.putExtra("data",String.valueOf(luas));
                    startActivity(intent);
                }
            }
        });
        vol.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (et_panjang.getText().toString().length()==0&et_lebar.getText().toString().length()==0&et_tinggi.getText().toString().length()==0){
                    et_panjang.setError("Required!");
                    et_lebar.setError("Required!");
                    et_tinggi.setError("Required!");
                }
                else if (et_lebar.getText().toString().length()==0&et_tinggi.getText().toString().length()==0){
                    et_lebar.setError("Required!");
                    et_tinggi.setError("Required!");
                }
                else if (et_panjang.getText().toString().length()==0&et_lebar.getText().toString().length()==0){
                    et_lebar.setError("Required!");
                    et_panjang.setError("Required!");
                }
                else if (et_panjang.getText().toString().length()==0&et_tinggi.getText().toString().length()==0){
                    et_tinggi.setError("Required!");
                    et_panjang.setError("Required!");
                }
                else if (et_panjang.getText().toString().length()==0){
                    et_panjang.setError("Required!");
                }
                else if (et_lebar.getText().toString().length()==0){
                    et_lebar.setError("Required!");
                }
                else if (et_tinggi.getText().toString().length()==0){
                    et_tinggi.setError("Required!");
                }
                else {
                    List<String>variabel=new ArrayList<>();
                    data(variabel);

                    String vo="Volume";
                    Double volume=p*l*t;

                    Intent intent=new Intent(getApplicationContext(),result.class);
                    intent.putExtra("char",String.valueOf(vo));
                    intent.putExtra("data",String.valueOf(volume));
                    startActivity(intent);
                }
            }
        });

    }
    private void data(List<String> variabel)
    {
        String panjang=et_panjang.getText().toString();
        p=Double.parseDouble(panjang);
        String lebar=et_lebar.getText().toString();
        l=Double.parseDouble(lebar);
        String tinggi=et_tinggi.getText().toString();
        t=Double.parseDouble(tinggi);
    }
}
